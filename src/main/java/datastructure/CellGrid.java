package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState initialState;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return grid.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if (column < 0 || column >= columns)
			throw new IndexOutOfBoundsException("Illegal column value");

        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if (column < 0 || column >= columns)
			throw new IndexOutOfBoundsException("Illegal column value");

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copiedCellGrid = new CellGrid(numRows(), numColumns(), initialState);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                CellState state = get(i, j);
                copiedCellGrid.set(i, j, state);
            }
        }
        return copiedCellGrid;
    }
}

