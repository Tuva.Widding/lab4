package cellular;

import java.util.Random;

import datastructure.IGrid;
import datastructure.CellGrid;


public class BriansBrain implements CellAutomaton {

    IGrid brain;

    public BriansBrain(int rows, int columns) {
		brain = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }
    
    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < brain.numRows(); row++) {
            for (int col = 0; col < brain.numColumns(); col++) {
                if (random.nextBoolean()) {
                    brain.set(row, col, CellState.ALIVE);
                } else {
                    brain.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return brain.get(row, column);
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid newBrain = brain.copy();
		for (int r = 0; r < numberOfRows(); r++) {
            for (int c = 0; c < numberOfColumns(); c++) {
        		CellState next = getNextCell(r, c);
				newBrain.set(r, c, next);
            }
        }
        brain = newBrain;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub
        CellState state = getCellState(row, col);
        if (state == CellState.ALIVE) {
            state = CellState.DYING;
        }
        else if (state == CellState.DYING) {
            state = CellState.DEAD;
        }
        else {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                state = CellState.ALIVE;
            }
            else {
                state = CellState.DEAD;
            }
        }
        return state;
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int countAlive = 0;
		int countDead = 0;
		for (int i = row -1; i <= row + 1; i++) {
            if (i<0 || i >= numberOfRows()) continue;
			for (int j = col -1; j <= col +1; j++) {
                if (j<0 || j >= numberOfColumns()) continue;
				CellState neighbourState = getCellState(i, j);
				if (neighbourState == CellState.ALIVE) {
					if (i == row && j == col) {
						continue;
					}
					countAlive++;
				}
				else {
					countDead++;
				}
			}
		}
		if (state == CellState.ALIVE) {
			return countAlive;
		}
		else {
			return countDead;
		}
	}


    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return brain.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return brain.numColumns();
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return brain;
    }
    
}   
